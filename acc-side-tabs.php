<?php
/**
* Plugin Name: Side Tabs
* Plugin URI: http://anothercoffee.net
* Description: A custom side tabs plugin.
* Version: 0.1
* Author: Anthony Lopez-Vito
* Author URI: http://anothercoffee.net
**/

/* 
 * For security as specified in
 * http://codex.wordpress.org/Writing_a_Plugin
 */
defined('ABSPATH') or die("No script kiddies please!");

/* 
 * Defs
 */
define( 'SIDETABS_VERSION', '0.1' );
define( 'SIDETABS_REQUIRED_WP_VERSION', '3.9' );
define( 'SIDETABS_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( 'SIDETABS_PLUGIN_NAME', trim( dirname( SIDETABS_PLUGIN_BASENAME ), '/' ) );
define( 'SIDETABS_PLUGIN_DIR', untrailingslashit( dirname( __FILE__ ) ) );
define( 'SIDETABS_PLUGIN_URL', untrailingslashit( plugins_url( '', __FILE__ ) ) );
define( 'SIDETABS_PLUGIN_MODULES_DIR', SIDETABS_PLUGIN_DIR . '/modules' );

/*
 * Register the Side Tabs Post Type
 */
function register_side_tabs() {
 
    $labels = array(
        'name' => _x( 'Side Tab', 'side_tabs' ),
        'singular_name' => _x( 'Side Tab', 'side_tabs' ),
        'add_new' => _x( 'Add New Side Tab', 'side_tabs' ),
        'add_new_item' => _x( 'Add New Side Tab', 'side_tabs' ),
        'edit_item' => _x( 'Edit Side Tab', 'side_tabs' ),
        'new_item' => _x( 'New Side Tab', 'side_tabs' ),
        'view_item' => _x( 'View Side Tab', 'side_tabs' ),
        'search_items' => _x( 'Search Side Tabs', 'side_tabs' ),
        'not_found' => _x( 'No side tabs found', 'side_tabs' ),
        'not_found_in_trash' => _x( 'No side tabs found in Trash', 'side_tabs' ),
        'parent_item_colon' => _x( 'Parent Position:', 'side_tabs' ),
        'menu_name' => _x( 'Side Tabs', 'side_tabs' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Side tab filterable by skill',
        'supports' => array( 'title', 'editor', 'thumbnail', 'revisions' ),
        'taxonomies' => array( 'skills' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-index-card',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type( 'side_tabs', $args );
}
 
add_action( 'init', 'register_side_tabs' );

/*
 * Section categories
 */
function sections_taxonomy() {
	$labels = array(
		'name' => 'Sections',
		'all_items' => 'All Sections',
		'edit_item' => 'Edit Section',
		'view_item' => 'View Section',
		'update_item' => 'Update Section',
		'add_new_item' => 'Add New Section',
		'new_item_name' => 'New Section Name',
		'parent_item_colon' => 'Parent Section:',
		'search_items' => 'Search Sections',
		'popular_items' => 'Popular Sections',
		'separate_items_with_commas' => 'Separate sections with commas',
	        'add_or_remove_items' => 'Add or remove sections',
		'choose_from_most_used' => 'Choose from the most used sections'
		);
	
    register_taxonomy(
        'sections',
		'side_tabs',
        array(
            'hierarchical' => true,
            'label' => 'Sections',
            'labels' => $labels,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'section',
                'with_front' => false
            )
        )
    );
}
add_action( 'init', 'sections_taxonomy');


/*
 * Link pages to side-tab posts by enabling sections categories for pages
 */
function enable_category_taxonomy_for_pages() {
    register_taxonomy_for_object_type('sections','page');
}
add_action( 'init', 'enable_category_taxonomy_for_pages' );


/*
 * Automatically create Side Tabs listing page
 */
function create_side_tabs_pages()
  {
   //post status and options
    $post = array(
          'comment_status' => 'closed',
          'ping_status' =>  'closed' ,
          'post_date' => date('Y-m-d H:i:s'),
          'post_name' => 'side_tabs',
          'post_status' => 'publish' ,
          'post_title' => 'Side Tabs',
          'post_type' => 'page',
    );
    //insert page and save the id
    $newvalue = wp_insert_post( $post, false );
    //save the id in the database
    update_option( 'side_tabs_page', $newvalue );
  }

// Activates function if plugin is activated
register_activation_hook( __FILE__, 'create_side_tabs_pages');


/*
 * Support meta box
 */
$meta_box = array(
    'id' => 'side_tabs-meta-box',
    'title' => 'Side tab details',
    'page' => 'side_tabs',
   	'context' => 'normal',
    'priority' => 'high',
    'args' => array(
        array(
            'name' => 'Subheading',
            'desc' => 'Enter subheading',
            'id' => 'st_subhead',
            'type' => 'text',
            'std' => ''
        ),
        array(
			'name' => 'Weight',
			'id' => 'st_weight',
			'type' => 'select',
			'options' => array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10')
		)
    )
);

// Add meta box
function side_tabs_add_box() {
    global $meta_box;
    add_meta_box(
			$meta_box['id'],
			$meta_box['title'],
			'side_tabs_callback',
			$meta_box['page'],
			$meta_box['context'],
			$meta_box['priority'],
			$meta_box['args']);
}
add_action('admin_menu', 'side_tabs_add_box');

// Callback function to show fields in meta box
function side_tabs_callback($post, $meta_box) {

	$st_subhead = get_post_meta($post->ID, "st_subhead", true);
	$st_weight = get_post_meta($post->ID, "st_weight", true);

    // Use nonce for verification
    echo '<input type="hidden" name="side_tabs_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
    echo '<table class="form-table">';
?>
<tr>
	<th style="width:20%"><label for="st_subhead">Subheading</label></th>
	<td><input type="text" name="st_subhead" id="st_subhead" value="<?php echo $st_subhead ?>" size="30" style="width:97%" /><br />Enter subheading</td>
</tr>
<tr>
	<th style="width:20%"><label for="wh_from_year">Weight</label></th>
	<td>
		<select name="st_weight" id="st_weight">
		<?php
    	foreach ($meta_box["args"][1]['options'] as $option) {
	    	echo '<option ';
			if($st_weight == $option) {
				echo ' selected="selected"';
			}
			echo '>', $option, '</option>';
		} ?>
    	</select>
	</td>
</tr>
	</table>
	<?php
}

// Save data from meta box
function side_tabs_save_data($post_id) {
    global $meta_box;
    // verify nonce
    if (!wp_verify_nonce($_POST['side_tabs_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
    foreach ($meta_box['args'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}
add_action('save_post', 'side_tabs_save_data');


/* 
 * Filter the single_template with our custom function
 */
function side_tabs_template($single) {
    global $wp_query, $post;

/* Checks for single template by post type */
if ($post->post_type == "side_tabs"){
    if(file_exists(SIDETABS_PLUGIN_DIR. '/single-side_tabs.php'))
	{
        return SIDETABS_PLUGIN_DIR . '/single-side_tabs.php';
	} else {
		error_log("file does not exist");		
	}
}
    return $single;
}

add_filter('single_template', 'side_tabs_template');


function side_tabs_archive_template( $archive_template ) {
	global $post;

	if ($post->post_type == "side_tabs"){
	    if(file_exists(SIDETABS_PLUGIN_DIR. '/archive-side_tabs.php'))
		{
	        return SIDETABS_PLUGIN_DIR . '/archive-side_tabs.php';
		} else {
			error_log("file does not exist");		
		}
	}
	return $archive_template;
}

add_filter( 'archive_template', 'side_tabs_archive_template' ) ;
