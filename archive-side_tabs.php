<?php get_header(); ?>

<div id="content">
<h1>Side tabs</h1>

					<?php
					/*
					 * Order the listing
					 */
					$posts = query_posts( $query_string . '&meta_key=st_weight&orderby=meta_value&order=dsc' ); 
					
					//$posts = query_posts($query);
					?>
					
					<?php if( $posts ) : ?>

					<?php 
					/*************************
					 * Customise the loop
					 */
					if(have_posts()) : ?>
						<div id="posts">
							<?php while(have_posts()): the_post(); ?>
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					                <div class="post-info">
					                    <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
					
										<div class="post-details">
										<?php if ( get_post_meta( get_the_ID(), 'st_subhead', true ) ) : ?>
											<?php echo '<strong>'.get_post_meta( get_the_ID(), 'st_subhead', true ).'</strong>'; ?>
										<?php endif; ?>
										<?php if ( get_post_meta( get_the_ID(), 'st_weight', true ) ) : ?>
											<?php echo get_post_meta( get_the_ID(), 'st_weight', true ); ?>
										<?php endif; ?>

										</div><!-- /.post-details -->
					                </div><!-- /.post-info -->

									<div class="post-excerpt">
					                    <?php $myExcerpt = get_the_excerpt(); $tags = array("<p>", "</p>"); $myExcerpt = str_replace($tags, "", $myExcerpt); echo $myExcerpt; ?>
					                    <a href="<?php the_permalink() ?>">Continue Reading</a>
					                </div><!-- /.post-excerpt -->

					            </div><!-- /.post -->
							<?php endwhile; ?>
						</div><!-- /#posts -->
					<?php endif ?>
					<?php 
					/*
					 * /Customise the loop
					 *************************/
					?>			

					<?php endif; ?>

	<div id="browsing">
		<div class="browse-nav">
			<div class="browse-left"><?php previous_posts_link('previous') ?>&nbsp;</div><!-- /.browse-left -->
			<div class="browse-mid"><div class="navigation"><?php if(function_exists('pagenavi')) { pagenavi(); } ?></div></div><!-- /.browse-mid -->
			<div class="browse-right"><?php next_posts_link('next') ?>&nbsp;</div><!-- /.browse-right -->
		</div><!-- /.browse-nav -->
		<div class="clear"></div>
	</div><!--  /#browsing -->
	
</div><!-- /#content -->

<?php get_footer(); ?>