<?php get_header(); global $post; ?>

	<div id="content">

		<div class="post-single">
			<h1><?php the_title(); ?></h1>
			<?php if ( get_post_meta( get_the_ID(), 'st_subhead', true ) ) : ?>
				<?php echo '<h2>'.get_post_meta( get_the_ID(), 'st_subhead', true ).'</h2>'; ?>
			<?php endif; ?>

			<p><strong>Weight:</strong><?php if ( get_post_meta( get_the_ID(), 'st_weight', true ) ) : ?>
				<?php echo get_post_meta( get_the_ID(), 'st_weight', true ); ?>
			<?php endif; ?></p>
			<hr />
            <div class="post-featured-image"><?php the_post_thumbnail('featured-image');  ?></div>
            
            <div class="clear"></div>

			<div class="entry">
	            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
				
                <div class="clear"></div>
            </div><!-- /.entry -->

        <?php endwhile; else: ?>

        		<p>Sorry, no posts matched your criteria.</p>

        <?php endif; ?>

        </div><!-- /.post-single -->

    </div><!-- /#content -->

<?php get_footer(); ?>