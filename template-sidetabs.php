<?php
/*
 * Template Name: Side Tabs
 *
 * Page template to display sidetab posts with the same section
 * category as the current page. Uses Side-Tabs custom plugin
 * to display tabbed areas.
 *
 */
?>

<?php
// File Security Check
if ( ! function_exists( 'wp' ) && ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page!' );
}

get_header(); // Loads the header.php template. ?>

		<?php get_template_part( 'loop-meta' ); // Loads the loop-meta.php template. ?>
    
		<?php get_template_part( 'breadcrumbs' ); // Loads the loop-meta.php template. ?>
		
		<div class="container">
			<div id="content" <?php if ( !is_single() && !is_page() && !is_attachment() ) echo 'class="hfeed"'; ?>	

				<?php get_template_part( 'loop' ); // Loads the loop.php template. ?>
				<?php get_template_part( 'loop-nav' ); // Loads the loop-nav.php template. ?>

				<?php
				/****************************************************
				 * Get section categories for this page and find
				 * side tab posts assigned to the same section
				 ****************************************************/
				$section_terms_for_page = get_the_terms($post->ID, 'sections' );
				$term_slug_array = array();
				if ($section_terms_for_page && ! is_wp_error($section_terms_for_page)) {
					foreach ($section_terms_for_page as $term) {
					    $term_slug_array[] = $term->slug;
					}
				}
				// We want all side_tab posts with the same sections,
				// ordered by the weight field in ascending order
				$args = array(
					'posts_per_page'   => -1,	//return maximum number
					'orderby'          => 'meta_value',
					'order'            => 'ASC',
					'include'          => '',
					'exclude'          => '',
					'meta_key'         => 'st_weight',
					'meta_value'       => '',
					'post_type'        => 'side_tabs',
					'post_mime_type'   => '',
					'post_parent'      => '',
					'post_status'      => '',
					'suppress_filters' => true,
					'tax_query' => array(
							array(
								'taxonomy' => 'sections',
								'field' => 'slug',
								// Set the relavent term slugs for the side-tab posts
								// that should appear on this page
								'terms' => $term_slug_array
							)
						)
					 );
				$posts_array = get_posts( $args );

				// Store the title and content for each side tab post
				$sidetabs_array = array();
				$tab_counter = 0;
				foreach ( $posts_array as $post )
				{
					setup_postdata( $post );
					$sidetabs_array[$tab_counter]['post_title']=$post->post_title;
					$sidetabs_array[$tab_counter]['post_content']=$post->post_content;					
					$tab_counter++;
				}
				wp_reset_postdata();
				
				/****************************************************
				 * Now display the matching side-tab posts on a
				 * tabbed user interface
				 ****************************************************/
				?>
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
					<?php
					// Populate the tabs with side-tab post title
					for ($index = 0; $index < count($sidetabs_array); $index++)
					{
						if($index == 0) {
							echo "<li class='active'>";
						} else {
							echo "<li>";
						}
						echo "<a href='#tab_".
									$index.
									"' data-toggle='tab'>".
									$sidetabs_array[$index]['post_title'].
									"</a></li>";
					}
					?>
                    </ul>
                    <div class="tab-content">
					<?php
					// Populate the tab content with side-tab post content
					for ($index = 0; $index < count($sidetabs_array); $index++)
					{
						if ($index==0) {
							echo "<div class='tab-pane active fade in' ";
						} else {
							echo "<div class='tab-pane fade' ";
						}
						echo"id='tab_".$index."'>".$sidetabs_array[$index]['post_content']."</div>";
					}	
					?>
                    </div>
                </div>
                </div>
			</div><!-- #content -->

		<?php 
		if ( maybe_woocommerce() )
			get_sidebar( 'woocommerce' ); // Loads the sidebar-woocommerce.php template.
		else 
			get_sidebar( 'primary' ); // Loads the sidebar-primary.php template. 
		?>
		</div><!-- .container -->
		
<?php get_footer(); // Loads the footer.php template. ?>