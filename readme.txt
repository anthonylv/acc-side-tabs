A simple WordPress Plugin that creates tabbed content on page content types
by Another Cup of Coffee Limited


This plugin was created for the specific needs of a client. YMMV.

* Creates custom side-tab post type
* Creates custom section taxonomy
* Add section taxonomy to page content type
* Displays side-tabs from side-tab post type on pages


First released 2015-02-05 by Anthony Lopez-Vito of Another Cup of Coffee Limited
http://anothercoffee.net


All code is released under The MIT License.
Please see LICENSE.txt.